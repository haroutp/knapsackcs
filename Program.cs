﻿using System;

namespace KnapsackLight
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(knapsackLight(10, 5, 6, 4, 8));//10
            Console.WriteLine(knapsackLight(10, 5, 6, 4, 9));//16
            Console.WriteLine(knapsackLight(5, 3, 7, 4, 6));//7
            Console.WriteLine(knapsackLight(10, 2, 11, 3, 1));//0
            Console.WriteLine(knapsackLight(2, 5, 3, 4, 5));//3
            Console.WriteLine(knapsackLight(15, 2, 20, 3, 2));//15
            Console.WriteLine(knapsackLight(4, 3, 3, 4, 4));//4
            Console.WriteLine(knapsackLight(3, 5, 3, 8, 10));//3
        }

        static int knapsackLight(int value1, int weight1, int value2, int weight2, int maxW) {
            
            if(weight1 + weight2 <= maxW){
                return value1 + value2;
            }

            if(maxW >= weight1 && maxW >= weight2){
                if(value1 >= value2){
                    return value1;
                }else{
                    return value2;
                }
            }

            if(maxW >= weight1){
                return value1;
            }
            if(maxW>= weight2){
                return value2;
            }
            return 0;
        }



    }
}
